export interface IConfig {
  hostname: string;
  path: string;
}

export default () => {
  return {
    hostname: 'platform.betterportal.cloud',
    path: '/packages.swarm.json'
  }
}