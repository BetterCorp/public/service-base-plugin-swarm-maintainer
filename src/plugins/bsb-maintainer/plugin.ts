import { CPlugin } from "@bettercorp/service-base/lib/ILib";
import { ServiceDeploymentNotification } from '../../lib';
import * as https from 'https';
import { IConfig } from './sec.config';


export class Plugin extends CPlugin<IConfig> {
  private getFullListOfCurrentPluginVersions(): Promise<Array<ServiceDeploymentNotification>> {
    return new Promise(async (resolve, reject) => {
      const options = {
        'method': 'GET',
        'hostname': (await this.getPluginConfig()).hostname,
        'path': (await this.getPluginConfig()).path,
        'headers': {},
        'maxRedirects': 5
      };

      https.request(options, (res) => {
        const chunks: any = [];

        res.on("data", (chunk) => chunks.push(chunk));

        res.on("end", () => {
          const body = Buffer.concat(chunks);
          resolve(JSON.parse(body.toString()));
        });

        res.on("error", (error) => reject(error));
      }).end();
    });
  }

  loaded(): Promise<void> {
    const self = this;
    return new Promise(async (resolve) => {
      self.getFullListOfCurrentPluginVersions().then(x => self.emitEvent('swarm-maintainer', 'images-updated', x)).catch(self.log.error);
      setInterval(() => {
        self.log.info('RUN FORCE-RESYNC');
        self.getFullListOfCurrentPluginVersions().then(x => self.emitEvent('swarm-maintainer', 'images-updated', x)).catch(self.log.fatal);
      }, (60 * 60 * 24) * 1000); // every 24h
      resolve();
    });
  }
}