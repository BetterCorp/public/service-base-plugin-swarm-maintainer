export interface ServiceUpdate {
  image: string;
  id: string;
}

export interface ServiceDeploymentNotification {
  image: string;
  version: string;
}

export interface DockerService {
  ID: string;
  Image: string;
  Mode: string;
  Name: string;
  Replicas: string;
}