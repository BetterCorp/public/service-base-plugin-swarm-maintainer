const fs = require('fs');
const path = require("path");
let CWD = process.cwd();

console.log(`Install CWD: ${ CWD }`);

if (CWD.indexOf("@bettercorp") >= 0) {
  CWD = path.join(CWD, "../../../");
}

console.log(`INSTALL SCRIPT FOR @bettercorp/service-base-plugin-swarm-maintainer in ${ CWD }`);

const secFile = path.join(CWD, `./sec.config.json`);

let readConfig = {};
if (fs.existsSync(secFile)) {
  readConfig = JSON.parse(fs.readFileSync(secFile).toString());
}
readConfig.debug = readConfig.debug || false;
readConfig.plugins = readConfig.plugins || {};
readConfig.deploymentProfiles = readConfig.deploymentProfiles || {};
readConfig.deploymentProfiles.default = readConfig.deploymentProfiles.default || {};

readConfig.deploymentProfiles.default["swarm-maintainer"] = readConfig.deploymentProfiles.default["swarm-maintainer"] || {};
readConfig.deploymentProfiles.default["swarm-maintainer"].mappedName = readConfig.deploymentProfiles.default["swarm-maintainer"].mappedName || "swarm-maintainer";
readConfig.deploymentProfiles.default["swarm-maintainer"].enabled = readConfig.deploymentProfiles.default["swarm-maintainer"].enabled || true;

readConfig.deploymentProfiles.default["bsb-maintainer"] = readConfig.deploymentProfiles.default["bsb-maintainer"] || {};
readConfig.deploymentProfiles.default["bsb-maintainer"].mappedName = readConfig.deploymentProfiles.default["bsb-maintainer"].mappedName || "bsb-maintainer";
readConfig.deploymentProfiles.default["bsb-maintainer"].enabled = readConfig.deploymentProfiles.default["bsb-maintainer"].enabled || true;
readConfig.plugins["bsb-maintainer"] = readConfig.plugins["bsb-maintainer"] || {};
readConfig.plugins["bsb-maintainer"].hostname = readConfig.plugins["bsb-maintainer"].hostname || 'platform.betterportal.cloud';
readConfig.plugins["bsb-maintainer"].path = readConfig.plugins["bsb-maintainer"].path || '/packages.swarm.json';

readConfig.deploymentProfiles.default["events-pubnub"] = readConfig.deploymentProfiles.default["events-pubnub"] || {};
readConfig.deploymentProfiles.default["events-pubnub"].mappedName = readConfig.deploymentProfiles.default["events-pubnub"].mappedName || "events-pubnub";
readConfig.deploymentProfiles.default["events-pubnub"].enabled = readConfig.deploymentProfiles.default["events-pubnub"].enabled || true;

readConfig.plugins = readConfig.plugins || {};
readConfig.plugins["events-pubnub"] = readConfig.plugins["events-pubnub"] || {};
readConfig.plugins["events-pubnub"].subscribeKey = readConfig.plugins["events-pubnub"].subscribeKey || "sub-c-6005ffdc-5642-11ec-9688-76c2d69ab9b3";

fs.writeFileSync(secFile, JSON.stringify(readConfig));

/*const packFile = path.join(CWD, `./package.json`);
let packConfig = JSON.parse(fs.readFileSync(packFile).toString());
packConfig.scripts.start = "node ./content/index.js";
fs.writeFileSync(packFile, JSON.stringify(packConfig));*/